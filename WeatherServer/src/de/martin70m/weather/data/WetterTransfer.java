package de.martin70m.weather.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import de.martin70m.common.io.FTPDownloader;
import de.martin70m.common.io.FileFinder;
import de.martin70m.common.io.ZipReader;
import de.martin70m.common.sql.MySqlConnection;

public class WetterTransfer {

	private static final String STATIONEN = "TU_Stundenwerte_Beschreibung_Stationen.txt";
	private static final String WETTERDATEN = "stundenwerte_TU_[ID]_akt.zip";
	private static final String LOCAL_DIRECTORY = "data"; // "C:/Temp/Wetterdaten";

	private static final String AIR_TEMPERATURE_RECENT = "/pub/CDC/observations_germany/climate/hourly/air_temperature/recent/";
	private static final String FTP_CDC_DWD_DE = "ftp-cdc.dwd.de";

	private static Set<String> geoCodes;

	public static void start(boolean withDownload) {

		int numberFiles = 0;
		long seconds = 0;

		try {
			selectMyGuests();
		} catch (SQLException se) {
			System.out.println(se.getMessage());
		}

		if (withDownload) {
			downloadFtpWeatherFiles(numberFiles, seconds);
		}

		List<String> stations = null;
		try {
			File infile = new File(LOCAL_DIRECTORY + "/" + STATIONEN);
			stations = readDataFromFile(infile);
		} catch (IOException fe) {
			System.out.println(fe.getMessage());
		}

		
		try {
			MySqlConnection mySqlDB = new MySqlConnection();
			try (final Connection conn = mySqlDB.getConnection()) {
				
				if (withDownload){
					insertIntoFtpTables(conn, numberFiles, seconds);
				}
					
				
				for (String station : stations) {
					StationDTO stationData = populateStationDTO(station);

					PreparedStatement prep = conn.prepareStatement("SELECT * FROM Station WHERE id = ?;");
					prep.setInt(1, stationData.getID());

					try (final ResultSet rs = prep.executeQuery()) {
						if (rs.next()) {
							updateStationTable(stationData, rs, conn);
						} else {

							insertIntoStationTable(stationData, conn);
						}
					}

					unzipAndInsertIntoMesswertTable(conn, stationData.getID());

				}

			}

		} catch (SQLException se) {
			System.out.println(se.getMessage());
		}

	}

	private static void selectMyGuests() throws SQLException {
		MySqlConnection mySqlDB = new MySqlConnection();
		Connection conn = mySqlDB.getConnection();
		PreparedStatement prep = conn.prepareStatement("SELECT * FROM MyGuests");
		ResultSet rs = prep.executeQuery();
		while (rs.next()) {
			System.out.println(rs.getString("email") + " - " + rs.getString("reg_date"));
		}

	}

	private static void downloadFtpWeatherFiles(int numberFiles, long seconds) {
		LocalTime startTime = LocalTime.now(ZoneId.of("Europe/Berlin"));

		try {
			FTPDownloader ftpDownloader = new FTPDownloader(FTP_CDC_DWD_DE, "anonymous", "");

			numberFiles = ftpDownloader.downloadFiles(AIR_TEMPERATURE_RECENT, LOCAL_DIRECTORY);
			if (numberFiles > 0)
				System.out.println("FTP File downloaded successfully");

			ftpDownloader.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		LocalTime endTime = LocalTime.now(ZoneId.of("Europe/Berlin"));

		Duration duration = Duration.between(startTime, endTime);

		seconds = duration.getSeconds();
	}

	private static void insertIntoFtpTables(Connection conn, int numberFiles, long seconds) throws SQLException {
		try (final PreparedStatement prep = conn.prepareStatement(
				"INSERT INTO FtpDownload (numberFiles, successful, duration, location) VALUES (?,?,?,?);")) {
			prep.setInt(1, numberFiles);
			prep.setString(2, "Y");
			prep.setLong(3, seconds);
			prep.setString(4, FTP_CDC_DWD_DE + AIR_TEMPERATURE_RECENT);
			prep.execute();
		}
	}

	private static StationDTO populateStationDTO(String station) {

		StationDTO stationData = new StationDTO();
		String[] data = Arrays.asList(station.split("[ ]")).stream().filter(str -> !str.isEmpty())
				.collect(Collectors.toList()).toArray(new String[0]);
		stationData.setID(new Integer(data[0]).intValue());
		stationData.setVonDatum(new Integer(data[1]).intValue());
		stationData.setBisDatum(new Integer(data[2]).intValue());
		stationData.setHeight(new Integer(data[3]).intValue());
		stationData.setLatitude(data[4]);
		stationData.setLongitude(data[5]);
		stationData.setName(data[6]);
		stationData.setLand(data[7]);

		return stationData;

	}

	private static void updateStationTable(StationDTO stationData, ResultSet rs, Connection conn) throws SQLException {

		if (stationData.getBisDatum() != rs.getLong("bisDatum")) {
			final PreparedStatement prep1 = conn.prepareStatement("UPDATE Station set bisDatum = ? WHERE id = ?;");
			prep1.setLong(1, stationData.getBisDatum());
			prep1.setInt(2, stationData.getID());
			prep1.execute();
			System.out.println(rs.getString("name") + " updated");

		} else {
			System.out.println(rs.getString("name"));
		}

	}

	private static void insertIntoStationTable(StationDTO stationData, Connection conn) throws SQLException {
		try (final PreparedStatement prep2 = conn.prepareStatement(
				"INSERT INTO Station (id, name, vonDatum, bisDatum, geoBreite, geoLaenge, hoehe, bundesland) VALUES (?,?,?,?,?,?,?,?);")) {
			prep2.setInt(1, stationData.getID());
			prep2.setString(2, stationData.getName());
			prep2.setLong(3, stationData.getVonDatum());
			prep2.setLong(4, stationData.getBisDatum());
			prep2.setString(5, stationData.getLatitude());
			prep2.setString(6, stationData.getLongitude());
			prep2.setInt(7, stationData.getHeight());
			prep2.setString(8, stationData.getLand());

			prep2.execute();
		}
	}

	private static List<String> readDataFromFile(File infile) throws IOException {
		List<String> allLines;
		allLines = Files.readAllLines(infile.toPath(), StandardCharsets.ISO_8859_1);
		List<String> badLines = new ArrayList<String>();

		if (allLines != null && !allLines.isEmpty()) {
			for (String line : allLines) {
				if (line.startsWith("---") || line.toUpperCase().startsWith("STATION"))
					badLines.add(line);
			}
		}
		for (String badLine : badLines) {
			allLines.remove(badLine);
		}
		return allLines;
	}

	public static void collectGeoCodesFromCSV() {

		geoCodes = new HashSet<>();

		String trackFile = "csv/track.csv";
		String csvFile = "csv/wines_data.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] geoCode = line.split(csvSplitBy);
				geoCodes.add(geoCode[0]);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		
		
		try {
			br = new BufferedReader(new FileReader(trackFile));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] geoCode = line.split(csvSplitBy);
				if (geoCode[1] == null || geoCode[1].isEmpty())
					continue;

				geoCodes.add(geoCode[10]);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void findStationByGeocodes() throws SQLException {

		MySqlConnection mySqlDB = new MySqlConnection();
		Connection conn = mySqlDB.getConnection();

		String longitude = "";
		String latitude = "";

		for (String item : geoCodes) {
			String[] longLat = item.split("\\s+");

			if (longLat.length > 1) {
				longitude = longLat[0];
				latitude = longLat[1];
			}

			PreparedStatement prep = conn
					.prepareStatement("SELECT * FROM Station WHERE geoBreite = ? and geoLaenge = ? ;");
			prep.setString(1, longitude.trim());
			prep.setString(2, latitude.trim());

			ResultSet rs = prep.executeQuery();
			if (rs.next()) {
				System.out.println("found = " + rs.getInt(1));
				int stationId = rs.getInt(1);

				// unzip to folder and then insert into messwert
				unzipAndInsertIntoMesswertTable(conn, stationId);
			}
		}
	}

	private static void unzipAndInsertIntoMesswertTable(Connection conn, int stationId) throws SQLException {

		String id = "0000" + stationId;
		while (id.length() > 5) {
			id = id.substring(1, id.length());
		}
		String filename = WETTERDATEN.replace("[ID]", id);
		File zipFile = new File(LOCAL_DIRECTORY, filename);

		// if it doesn't exist, ignore
		if (!zipFile.exists()) {
			
		}

		// create folder if not exist
		File unzippedFolder = new File(LOCAL_DIRECTORY + "/" + id);
		if (!unzippedFolder.exists()) {
			unzippedFolder.mkdir();
		}

		int filecounter = ZipReader.upzip(LOCAL_DIRECTORY + "/" + filename, LOCAL_DIRECTORY + "/" + id);
		System.out.println(filename + " extraced to " + filecounter + " files.");
		// produkt_tu_stunde

		if (filecounter > 0) {
			String unzippedDir = LOCAL_DIRECTORY + "/" + id;
			String filename1 = FileFinder.find("produkt_tu_stunde", unzippedDir);
			File infile = new File(unzippedDir + "/" + filename1);
			List<String> temperatures = null;
			try {
				int alteStationsID = 0;
				long maxDatum = 0;
				int maxUhrzeit = 0;
				temperatures = readDataFromFile(infile);
				for (String temperature : temperatures) {
					System.out.println(temperature);
					MesswertDTO messwert = new MesswertDTO();
					List<String> data1 = Arrays.asList(temperature.split(";"));
					messwert.setStationID(new Integer(data1.get(0).trim()).intValue());
					if (alteStationsID != messwert.getStationID()) {
						try (final PreparedStatement prep3 = conn.prepareStatement(
								"SELECT max(datum) as maxdatum, max(uhrzeit) as maxuhrzeit FROM Messwert WHERE stationid = ?;")) {
							prep3.setInt(1, messwert.getStationID());
							try (final ResultSet rs = prep3.executeQuery()) {
								if (rs.next()) {
									maxDatum = rs.getLong("maxdatum");
									maxUhrzeit = rs.getInt("maxuhrzeit");
									alteStationsID = messwert.getStationID();
								}
							}

						}
					}
					long datetime = new Integer(data1.get(1).trim()).intValue();
					messwert.setDate(datetime / 100);
					long time = datetime - messwert.getDate() * 100;
					messwert.setHour((int) time);

					if (messwert.getDate() > maxDatum
							|| messwert.getDate() == maxDatum && messwert.getHour() > maxUhrzeit) {
						messwert.setTemperatur(data1.get(3).trim());
						messwert.setHumidity(data1.get(4).trim());
						try (final PreparedStatement prep3 = conn.prepareStatement(
								"SELECT count(*) as anzahl FROM Messwert WHERE stationid = ? AND datum = ? and uhrzeit = ?;")) {
							prep3.setInt(1, messwert.getStationID());
							prep3.setLong(2, messwert.getDate());
							prep3.setInt(3, messwert.getHour());
							try (final ResultSet rs = prep3.executeQuery()) {
								if (rs.next()) {
									if (rs.getLong("anzahl") == 0) {
										try (final PreparedStatement prep2 = conn.prepareStatement(
												"INSERT INTO Messwert (stationid, datum, uhrzeit, temperatur, luftfeuchte) VALUES (?,?,?,?,?);")) {
											prep2.setInt(1, messwert.getStationID());
											prep2.setLong(2, messwert.getDate());
											prep2.setInt(3, messwert.getHour());
											prep2.setString(4, messwert.getTemperatur());
											prep2.setString(5, messwert.getHumidity());

											prep2.execute();
											System.out.println("inserted to database");
										}
									}
								}
							}
						}
					}

				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
