Stations_ID;Stationsname;Geo. Laenge [Grad];Geo. Breite [Grad];Stationshoehe [m];Geberhoehe ueber Grund [m];Von_Datum;Bis_Datum;Geraetetyp Name;Messverfahren;eor;
71;Albstadt-Lautlingen;8.96;48.22;703;2;19861101;19940930;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
71;Albstadt-Lautlingen;8.96;48.21;698;2;19941101;20031231;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
71;Albstadt-Badkap;8.98;48.22;759;2;20091201;;Feuchtesonde HMP45D;Feuchtemessung, elektr.;eor;
generiert: 30.11.2017 --  Deutscher Wetterdienst  --
