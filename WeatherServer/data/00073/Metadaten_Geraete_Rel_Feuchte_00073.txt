Stations_ID;Stationsname;Geo. Laenge [Grad];Geo. Breite [Grad];Stationshoehe [m];Geberhoehe ueber Grund [m];Von_Datum;Bis_Datum;Geraetetyp Name;Messverfahren;eor;
73;Aldersbach/Aidenbach;;;;;;19590228;Ger�tetyp unbekannt;Feuchteregistrieung, konv.;eor;
73;Aldersbach/Aidenbach;13.09;48.57;351;2;19590301;19620524;Hygrograph nach Frankenberg;Feuchteregistrieung, konv.;eor;
73;Aldersbach/Aidenbach;13.08;48.57;335;2;19620525;19790831;Hygrograph nach Frankenberg;Feuchteregistrieung, konv.;eor;
73;Aldersbach/Aidenbach;13.08;48.57;335;2;19750501;19790430;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
73;Aldersbach;13.09;48.59;325;2;19790901;20070331;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
73;Aldersbach-Kriestorf;13.05;48.62;340;2;20070401;20120601;Feuchtesonde HMP45D;Feuchtemessung, elektr.;eor;
73;Aldersbach-Kriestorf;13.05;48.62;340;2;20120605;20151104;Feuchtesonde HMP45D;Feuchtemessung, elektr.;eor;
73;Aldersbach-Kriestorf;13.05;48.62;340;2;20151105;;EE33;Feuchtemessung, elektr.;eor;
generiert: 30.11.2017 --  Deutscher Wetterdienst  --
