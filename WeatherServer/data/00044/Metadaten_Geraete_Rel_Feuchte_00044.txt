Stations_ID;Stationsname;Geo. Laenge [Grad];Geo. Breite [Grad];Stationshoehe [m];Geberhoehe ueber Grund [m];Von_Datum;Bis_Datum;Geraetetyp Name;Messverfahren;eor;
44;Ahlhorn;;;;;;19710228;Gerätetyp unbekannt;Feuchteregistrieung, konv.;eor;
44;Ahlhorn;8.23;52.88;48;2;19710301;19711130;Hygrograph nach Frankenberg;Feuchteregistrieung, konv.;eor;
44;Ahlhorn;8.23;52.88;48;2;19711201;19930630;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
44;Großenkneten-Ahlhorn;8.22;52.92;42;2;19930701;20070331;Thermohygrograph;Temperatur- und Feuchtereg., konv.;eor;
44;Großenkneten-Ahlhorn;8.22;52.92;42;2;20070208;20070331;Feuchtesonde HMP45D;Feuchtemessung, elektr.;eor;
44;Großenkneten;8.24;52.93;43.5;2;20070401;;Feuchtesonde HMP45D;Feuchtemessung, elektr.;eor;
generiert: 30.11.2017 --  Deutscher Wetterdienst  --
